# dxf-14-otp

Just a slight modification to the Inkscape core dxf14 output extension

Allows Inkscape commmand line preprocessing before export as DXF14

The default is `select-all:all;clone-unlink-recursively;object-to-path;`

Which converts all objects to paths before saving (no more missing text :) )

Appears under Desktop Cutting Plotter (AutoCAD DXF R14) OTP (*.dxf) in the `SaveAs` dropdown dialogue.

This preprocessing does not affect the current document on canvas, only the saved document.

-----------------------------------------------------------------------------------------------------

Also adds:

Extensions>Run Command Line Action

This allows any command line action to be tested on the current document so you can experiment

**  That means if you do save as svg after experimenting, changes will be saved. So backup first **

