#!/usr/bin/env python
# coding=utf-8
#
# Copyright (C) [2023] [Matt Cottam], [mpcottam@raincloud.co.uk]
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#

##############################################################################
# Run command line action, simple script to allow another extension to
# either process an svg document by command line or allow an export extension
# to preprocess the svg via command line.
# An Inkscape 1.2.1+ extension
# Appears under Extensions>Run Command Line Action
##############################################################################


import inkex

import tempfile, os, shutil
from uuid import uuid4

def process_svg(self, action_string, method):

    temp_folder = tempfile.mkdtemp()

    # Create a random filename for svg
    svg_temp_filepath = os.path.join(temp_folder, f'original_{str(uuid4())}.svg')

    with open(svg_temp_filepath, 'w') as output_file:
        svg_string = self.svg.tostring().decode('utf-8')
        output_file.write(svg_string)

    processed_svg_temp_filepath = os.path.join(temp_folder, f'processed_{str(uuid4())}.svg')

    my_actions = '--actions='

    # Let's make sure the action_string ends with a semicolon ;

    if action_string[-1] != ';':
        action_string += ';'

    # Add export to the action string
    # The FileSave verb no longer exists in Inkscape 1.2, so use export instead

    export_action_string = my_actions + f'export-filename:{processed_svg_temp_filepath};{action_string}export-do;'

    # Run the command line
    inkex.command.inkscape(svg_temp_filepath, export_action_string)

    # Replace the current document with the processed document
    with open(processed_svg_temp_filepath, 'rb') as processed_svg:
        # For standalone
        if method == 'standalone':
            self.document = inkex.elements.load_svg(processed_svg)
        # For dxf output
        if method == 'output':
            self.document = self.load(processed_svg)
        # inkex.errormsg(self.svg.tostring())

    # This removes the temp folder
    # comment out with # if you want to keep it
    shutil.rmtree(temp_folder)


class RunCommandLineAction(inkex.EffectExtension):

    def add_arguments(self, pars):

        # Basic Page
        pars.add_argument("--run_command_line_action_notebook", type=str, dest="run_command_line_action_notebook", default=0)

        pars.add_argument("--action_string_radio", type=str, dest="action_string_radio", default='')
        pars.add_argument("--action_string1", type=str, dest="action_string1", default='select-all:all;clone-unlink-recursively;object-to-path;')
        pars.add_argument("--action_string2", type=str, dest="action_string2", default='')
        pars.add_argument("--action_string3", type=str, dest="action_string3", default='')
        pars.add_argument("--action_string4", type=str, dest="action_string4", default='')
        pars.add_argument("--action_string5", type=str, dest="action_string5", default='')
        pars.add_argument("--action_string6", type=str, dest="action_string6", default='')

    
    def effect(self):

        # Let's get the action string from the user selection / textboxes
        selected_action_string = 'action_string' + self.options.action_string_radio

        action_string = getattr(self.options, selected_action_string)

        process_svg(self, action_string, 'standalone')


if __name__ == '__main__':
    RunCommandLineAction().run()
